import getopt, sys, io, os
from hash_function import hashFunction
from chunk import *
from methods import *

paramMUsed = False
inputFile = None
outputFile = None
chunk = 2000000 #size of the chunk

try:
	opts, args = getopt.getopt(sys.argv[1:], "m:i:o:s:")
except getopt.GetoptError as err:
	print (str(err))
	sys.exit(2)

for o, a in opts:
	if o in "-m":
		if int(a) in [1, 2, 3]:
			m = a
			paramMUsed = True
		else: 
			print ("You specified wrong m parameter. You must specify length of hash function by: -m n \nwhere n is 1, 2 or 3, which are as follows 64b, 96b or 128b. ")
			sys.exit(2)
	elif o in ("-i"):
		inputFile = a
	elif o in ("-o"):
		outputFile = a
	elif o in ("-s"):
		chunk = int(a)

if not paramMUsed:
    print ("You must specify length of hash function by: -m n \nwhere n is 1, 2 or 3, which are as follows 64b, 96b or 128b.")
    sys.exit(2)	

resultHash = cHash(chunk, inputFile, m)

showResult(outputFile, resultHash)