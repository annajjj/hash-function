import getopt, sys, io, os
from hash_function import hashFunction

def openFile(file_name):
    try:	
        iFile = open(file_name)
        data = iFile.read()
    finally:
        iFile.close()
        return data

def writeToFile(file_name, data):
    try:
        oFile = open(file_name, "w")
        oFile.write(data)
    finally:
        oFile.close()

def writeToFile2(file, data):
    with open(file, 'a') as outB:
        outB.write(data)
        outB.write("\n")
        
def hashChunk(file_name, m, chunk1, subres = "resultX.txt", removeSubresFile = True):
	open('resultX.txt', 'w').close()
	with open(file_name, 'r') as f:
		while True:
			chunk = f.read(chunk1)

			if not chunk:
				data = openFile("resultX.txt")
				cHash = hashFunction(data, int(m))
				if removeSubresFile:
					os.remove("resultX.txt")
				return cHash

			cHash = hashFunction(chunk, int(m))
			writeToFile2(subres, cHash)

def inputFileCheck(inputFile):
    if inputFile == None:
        print("Enter input data")
        data = input("->")
        dataSize = len(data)
    else:
        try:
            dataSize = os.path.getsize(inputFile)
            data = ''
        except:
            print ("File does not exist, hash will not be calculated")
            sys.exit(2)
    return data, dataSize

def showResult(outputFile, hashResult):
    if outputFile == None:
        print("Your hash: ", hashResult)
    else:
        print("Hash calculated successfuly and saved in file ", outputFile)
        writeToFile(outputFile, hashResult)

def cHash (chunk, inputFile, m):
    chunking = ""
    data, dataSize = inputFileCheck(inputFile)
    if dataSize == 0:
        print ("Input data is empty. Hash will not be calculated. ")
        sys.exit(2)

    elif dataSize <= chunk:
        if inputFile != None:
            data = openFile(inputFile)
        hashResult = hashFunction(data, int(m))
        
    else:
        print("Input data is bigger than: ", chunk, "Program must do extra calculations. Please be patient")
        if inputFile != None:
            chunking = inputFile
        else:
            writeToFile("dataTmp.txt", data)
            chunking = "dataTmp.txt"

        hashResult = hashChunk(chunking, m, chunk)               

    if chunking == "dataTmp.txt":
        os.remove(chunking)

    return hashResult


def toBinary(data):
    output=[]
    buffor=[]
    dataDec=[]
    for i in range(0, int(len(data))):
        dataDec.append(ord(data[i]))
    
    for i in range(0, len(dataDec)):
        while(dataDec[i]!=0):
            buffor.insert(0, int(dataDec[i]) % 2)
            dataDec[i] = int(dataDec[i]/ 2)
            if dataDec[i]==0:
                for j in range(0, 8-len(buffor)):
                   buffor.insert(0, 0)
        output.append(buffor)
        buffor = []
    return output