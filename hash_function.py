import binascii

def countDataSum(data):
	dataSum = 0
	for i in range(0, len(data)):
		dataSum += ord(data[i])
	return dataSum

def toHex(data):
	dataTmp = []
	for i in range(0, len(data)):
		dataTmp.append(hex(data[i])[2:])
		if len(dataTmp[i])<2:
			dataTmp[i]='0' + dataTmp[i]
	return ''.join(dataTmp).upper()

def hashFunction(data, hashLengthParam):
	hashLength = (hashLengthParam + 1)*32
	buffor = []
	dataLen = len(data)
	whereChar = int(hashLength/dataLen)
	j=0
	if dataLen < hashLength:
		for i in range(0, hashLength):
			if i%whereChar==0 and j<dataLen:
				buffor.append(data[j])
				j+=1
			else:
				buffor.append('0')
	elif dataLen >= hashLength:
		nadmiar = dataLen % hashLength
		charsPerBufforek = int((dataLen - nadmiar)/hashLength)
		x=0
		for i in range(0, hashLength):
			buffor.append('')
			for j in range(0,charsPerBufforek):
				if(x<dataLen):
					buffor[i]+=data[x]
					x+=1
			if nadmiar!=0 and i%int(hashLength/nadmiar)==0 and x<dataLen:
				buffor[i]+=data[x]
				x+=1

	dataSum = countDataSum(data)

	bufforTransformed = []
	bufforAscii = [0] * hashLength
	for i in range(0, hashLength):
		for j in range(0, len(buffor[i])):
			bufforAscii[i]+=ord(buffor[i][j])

		bufforTransformed.append(((bufforAscii[i]+1)*dataSum) % 256)		

	bufforOut = []
	for i in range(0, hashLength):
		if i==0:
			bufforOut.append(bufforTransformed[0])
		else:
			bufforOut.append(bufforOut[len(bufforOut) - 1] ^ bufforTransformed[i] ^ i)

	result = toHex(bufforOut)

	return result
	
