import getopt, sys, io, os
from methods import *
import random
import string


inputFile = 'w.txt'
chunk = 100000
hashes = []
data = ''
resultFile = "test-result.txt"
changes = 1
loop = 50
for j in range(0,loop):
    hashes.append([])
    for i in range(1,4):
        hashes[j].append(cHash(chunk, inputFile, i))
    data = list(openFile(inputFile))
    for q in range(0,changes):
        index = random.randint(0,len(data)-1)
        if string.printable.index(data[index])==0:
            data[index] = string.printable[string.printable.index(data[index])+1]
        else:
            data[index] = string.printable[string.printable.index(data[index])-1]

    f = open(inputFile, 'w')
    # print(''.join(data))
    # print('\n')
    f.write(''.join(data))
    f.close()
# print(hashes)
changePercent = []
count = 0

for j in range(1,loop):
    for i in range (1,4):
        count = 0
        for k in range(0,(1+i)*32):
            for q in range(0,8):
                if toBinary(list(hashes[j-1][i-1])[k])[0][q] != toBinary(list(hashes[j][i-1])[k])[0][q]:
                    count+=1
        changePercent.append(float(count)/((1+i)*32*8))
with open(resultFile, "w"):
        pass
with open(resultFile, "a") as myfile:
    for i in range(0,len(changePercent)):
        myfile.write(str(round(changePercent[i],2)))
        myfile.write(' ')
        if i%3==2:
            myfile.write('\n')

same = False
for i in range(0,len(hashes)):
    for j in range(1, len(hashes)):
        if i!=j:
            if hashes[i] == hashes[j]:
                same = True
print(same)
