import unittest, os
from methods import *
from hash_function import hashFunction

class TestStringMethods(unittest.TestCase):

    def test_equalHash(self):
        hash1 = cHash(1000, "test-text.txt", 1)
        hash2 = "22C121C026C125C02AC13FD638D73BD624D727D6A85FAB5EA45FA75EA05F13EE2CEF2FEE28EF2BEE26ED25EC22ED21EC3EED8150865185508A5189508E518D50"
        self.assertEqual(hash1, hash2)

    def test_diffrentHash(self):
        hash1 = cHash(1000, "test-text.txt", 1)
        hash2 = cHash(1000, "test-text2.txt", 1)
        self.assertNotEqual(hash1, hash2)

    def test_equalFromcHashAndFromhashFunction(self):
        hash1 = cHash(1000, "test-text.txt", 1)
        hash2 = hashFunction("Python", 1)
        self.assertEqual(hash1, hash2)

    def test_hashChunk(self):
        hash1 = hashChunk("test-text.txt", 1, 2, "resultX.txt", False)
        dataSize = os.path.getsize("resultX.txt")
        os.remove("resultX.txt")
        self.assertEqual(int(dataSize/128), 3)

    def test_HashInOutputFile(self):
        hash1 = cHash(1000, "test-text.txt", 1)
        showResult("tmp.txt", hash1)
        hash2 = openFile("tmp.txt")
        os.remove("tmp.txt")
        self.assertEqual(hash1, hash2)

if __name__ == '__main__':
    unittest.main()